﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GenerateTexture
{
    public partial class MainWindow : Window
    {
        private List<ImageSquarePart> _squares = new List<ImageSquarePart>();
        private Bitmap _bitmap;
        private Uri _bitmapUri;
        private bool _isAutoJoin = false;
        private Uri _templatePath = new Uri("pack://application:,,,/Images/template.png");

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ConvertImageToMatrix()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    int x1 = (int)ImageControl.Width / 4 * i;
                    int x2 = x1 + ((int)ImageControl.Width / 4);
                    int y1 = (int)ImageControl.Height / 4 * j;
                    int y2 = y1 + ((int)ImageControl.Height / 4);
                    ImageSquarePart part = new ImageSquarePart()
                    {
                        X1 = x1,
                        Y1 = y1,
                        X2 = x2,
                        Y2 = y2,
                        i = i,
                        j = j
                    };
                    part.color = _bitmap.GetPixel(part.X, part.Y);

                    _squares.Add(part);
                }
            }
        }

        private void ConvertMatrixToImage()
        {
            foreach (ImageSquarePart part in _squares)
            {
                SetSquareColorToBitmap(part.X1, part.Y1, part.X2, part.Y2, part.color);
            }
        }

        private void SetSquareColorToBitmap(int x1, int y1, int x2, int y2, Color color)
        {
            for (int x = x1; x < x2; x++)
            {
                for (int y = y1; y < y2; y++)
                {
                    _bitmap.SetPixel(x, y, color);
                }
            }
        }

        private void OpenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "PNG Files (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                _bitmapUri = new Uri(op.FileName);
                var bitmapImage = new BitmapImage();
                using (var fs = new FileStream(op.FileName, FileMode.Open, FileAccess.Read))
                {
                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = fs;
                    bitmapImage.EndInit();
                }

                _bitmap = BitmapUtils.BitmapImage2Bitmap(bitmapImage);
                ImageControl.Source = bitmapImage;

                ConvertImageToMatrix();
            }
        }

        private void NewMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var bitmapImage = new BitmapImage(_templatePath);
            _bitmap = BitmapUtils.BitmapImage2Bitmap(bitmapImage);
            ImageControl.Source = bitmapImage;
            ConvertImageToMatrix();
        }

        private void SaveMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (_bitmapUri == null)
            {
                SaveAs();
            }
            else
            {
                Save();
            }
        }

        private void Save()
        {
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)ImageControl.RenderSize.Width,
                                    (int)ImageControl.RenderSize.Height, 96d, 96d, System.Windows.Media.PixelFormats.Default);
            rtb.Render(ImageControl);

            var crop = new CroppedBitmap(rtb, new Int32Rect(0, 0, 32, 32));

            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(crop));

            using (var fs = System.IO.File.OpenWrite(_bitmapUri.AbsolutePath))
            {
                pngEncoder.Save(fs);
            }
        }

        private void SaveAs()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Save the picture";
            saveFileDialog.Filter = "PNG Files (*.png)|*.png";
            if (saveFileDialog.ShowDialog() == true)
            {
                _bitmapUri = new Uri(saveFileDialog.FileName);
            }
            Save();
        }

        private void SaveAsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private System.Drawing.Point GetScaledPointFromImage(System.Windows.Point p)
        {
            System.Windows.Point relativePoint = ImageControl.TransformToAncestor(MainWindowName).Transform(new System.Windows.Point(0, 0));
            System.Windows.Point clickPoint = new System.Windows.Point(p.X - relativePoint.X, p.Y - relativePoint.Y);
            int imageHeight = (int)ImageControl.Height;
            int imageWidth = (int)ImageControl.Width;
            int viewboxImageHeight = (int)ViewboxImageControl.Height;
            int viewboxImageWidth = (int)ViewboxImageControl.Width;
            int imageHeightScale = viewboxImageHeight / imageHeight;
            int imageWidthScale = viewboxImageWidth / imageWidth;
            return new System.Drawing.Point((int)Math.Round(clickPoint.X / imageWidthScale, 0), (int)Math.Round(clickPoint.Y / imageHeightScale, 0));
        }

        private ImageSquarePart GetSquareByImagePoint(System.Drawing.Point point)
        {
            return _squares.Find(square => square.isPointInSquare(point));
        }

        private void FillInNeighbors(ImageSquarePart part, Color newColor, Color prevColor)
        {
            ImageSquarePart left = _squares.Find(square => square.i == part.i - 1 && square.j == part.j);
            ImageSquarePart right = _squares.Find(square => square.i == part.i + 1 && square.j == part.j);
            ImageSquarePart up = _squares.Find(square => square.j == part.j - 1 && square.i == part.i);
            ImageSquarePart down = _squares.Find(square => square.j == part.j + 1 && square.i == part.i);

            part.color = newColor;

            if (left != null && left.color == prevColor)
            {
                FillInNeighbors(left, newColor, prevColor);
            }

            if (right != null && right.color == prevColor)
            {
                FillInNeighbors(right, newColor, prevColor);
            }

            if (up != null && up.color == prevColor)
            {
                FillInNeighbors(up, newColor, prevColor);
            }

            if (down != null && down.color == prevColor)
            {
                FillInNeighbors(down, newColor, prevColor);
            }            
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            _isAutoJoin = AutoJoinCheckBox.IsChecked == true;
        }

        private void ImageControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
            {
                System.Windows.Point p = e.GetPosition(this);
                System.Drawing.Point imagePoint = GetScaledPointFromImage(p);
                ImageSquarePart targetPart = GetSquareByImagePoint(imagePoint);

                ColorPicker picker = new ColorPicker();
                picker.SetColor(Color.FromArgb(255, 0, 255, 0));
                picker.ShowDialog();

                if (_isAutoJoin)
                {
                    Color prevColor = targetPart.color;
                    FillInNeighbors(targetPart, picker.GetColor(), prevColor);
                }
                else
                {
                    targetPart.color = picker.GetColor();
                }

                ConvertMatrixToImage();
                ImageControl.Source = BitmapUtils.Bitmap2BitmapImage2(_bitmap);
            }
        }

        private void ImageControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
            {
                System.Windows.Point p = e.GetPosition(this);
                System.Drawing.Point imagePoint = GetScaledPointFromImage(p);
                ImageSquarePart targetPart = GetSquareByImagePoint(imagePoint);

                System.Windows.Forms.ColorDialog colorDialog = new System.Windows.Forms.ColorDialog();
                colorDialog.Color = targetPart.color;
                if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (_isAutoJoin)
                    {
                        Color prevColor = targetPart.color;
                        FillInNeighbors(targetPart, colorDialog.Color, prevColor);
                    }
                    else
                    {
                        targetPart.color = colorDialog.Color;
                    }

                    ConvertMatrixToImage();
                    ImageControl.Source = BitmapUtils.Bitmap2BitmapImage2(_bitmap);
                }
            }
        }
    }
}
