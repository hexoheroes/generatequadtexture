﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GenerateTexture
{
    public class ImageSquarePart
    {
        public Color color;
        public int X1;
        public int Y1;
        public int X2;
        public int Y2;
        public int i;
        public int j;
        public int X => (X2 - X1) / 2 + X1;
        public int Y => (Y2 - Y1) / 2 + Y1;

        public bool isPointInSquare(Point point)
        {
            return point.X >= X1 && point.X <= X2 && point.Y >= Y1 && point.Y <= Y2;
        }
    }
}
