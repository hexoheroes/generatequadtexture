﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
//using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GenerateTexture
{
    public partial class ColorPicker : Window
    {
        private Bitmap _gradientBitmap;
        private Uri _gradientTemplatePath = new Uri("pack://application:,,,/Images/gradient_template.png");
        private Bitmap _hueColorsBitmap;
        private Uri _hueColorTemplatePath = new Uri("pack://application:,,,/Images/hue_color_template.png");
        private Bitmap _newColorsBitmap;
        private Uri _newColorTemplatePath = new Uri("pack://application:,,,/Images/new_color_template.png");
        private Bitmap _currentColorsBitmap;
        private Uri _currentColorTemplatePath = new Uri("pack://application:,,,/Images/current_color_template.png");

        private List<Color> _hueColors = new List<Color>();
        private Color[,] _gradientColors = new Color[256, 256];

        private int _selectedHueColorIndex = 0;
        private int[] _selectedGradientIndices = new int[]{ 255, 0 };

        private Color _currentColor = Color.Empty;
        private Color _selectedColor;

        public ColorPicker()
        {
            InitializeComponent();

            var bitmapImage = new BitmapImage(_gradientTemplatePath);
            _gradientBitmap = BitmapUtils.BitmapImage2Bitmap(bitmapImage);

            bitmapImage = new BitmapImage(_hueColorTemplatePath);
            _hueColorsBitmap = BitmapUtils.BitmapImage2Bitmap(bitmapImage);

            bitmapImage = new BitmapImage(_newColorTemplatePath);
            _newColorsBitmap = BitmapUtils.BitmapImage2Bitmap(bitmapImage);

            bitmapImage = new BitmapImage(_currentColorTemplatePath);
            _currentColorsBitmap = BitmapUtils.BitmapImage2Bitmap(bitmapImage);

            GenerateHueColors();
            DrawHueColorsBitmap();
            ColorGradient.Source = BitmapUtils.Bitmap2BitmapImage2(_hueColorsBitmap);

            DrawGradient();
            ChangeColor(GetGradientColor(), ChangesSource.GRADIENT);
        }

        public void SetColor(Color color)
        {
            ChangeColor(color, ChangesSource.EXTERNAL);
        }

        public Color GetColor()
        {
            return _selectedColor;
        }

        private void ChangeColor(Color newColor, ChangesSource source)
        {
            if (_currentColor == Color.Empty)
            {
                _currentColor = newColor;
                DrawNewColorBitmap();
            }

            _selectedColor = newColor;
            DrawCurrentColorBitmap();
            switch (source)
            {
                case ChangesSource.GRADIENT:
                {
                    SetRGBValues();
                    SetHexValue();
                    break;
                }
                case ChangesSource.RGB:
                {
                    SetNearestHueColor(newColor.GetHue());
                    DrawGradient();
                    SetHexValue();
                    break;
                }
                case ChangesSource.HEX:
                {
                    SetNearestHueColor(newColor.GetHue());
                    DrawGradient();
                    SetRGBValues();
                    break;
                }
                case ChangesSource.EXTERNAL:
                {
                    DrawNewColorBitmap();

                    SetNearestHueColor(newColor.GetHue());
                    DrawGradient();
                    SetRGBValues();
                    SetHexValue();
                    break;
                }
            }
        }

        private void DrawCurrentColorBitmap()
        {
            for (int i = 0; i < 35; i++)
            {
                for (int j = 0; j < 70; j++)
                {
                    _currentColorsBitmap.SetPixel(j, i, GetColor());
                }
            }
            CurrentColor.Source = BitmapUtils.Bitmap2BitmapImage2(_currentColorsBitmap);
        }

        private void DrawNewColorBitmap()
        {
            for (int i = 0; i < 35; i++)
            {
                for (int j = 0; j < 70; j++)
                {
                    _newColorsBitmap.SetPixel(j, i, GetColor());
                }
            }
            NewColor.Source = BitmapUtils.Bitmap2BitmapImage2(_newColorsBitmap);
        }

        private Color GetHueColor()
        {
            return _hueColors[_hueColors.Count / 256 * _selectedHueColorIndex];
        }

        private void SetNearestHueColor(float hue)
        {
            _selectedHueColorIndex = (int)(256.0f / 360 * (360 - hue));
        }

        private Color GetGradientColor()
        {
            return _gradientColors[_selectedGradientIndices[1], _selectedGradientIndices[0]];
        }

        private void DrawGradient()
        {
            GenerateGradient();
            DrawGradientBitmap();
            Gradient.Source = BitmapUtils.Bitmap2BitmapImage2(_gradientBitmap);
        }

        private void SetHexValue()
        {
            HexTextBox.Text = ColorToHtml(GetColor());
        }

        private void SetRGBValues()
        {
            RTextBox.Text = GetColor().R.ToString();
            GTextBox.Text = GetColor().G.ToString();
            BTextBox.Text = GetColor().B.ToString();
        }

        private string ColorToHtml(Color color)
        {
            return ColorTranslator.ToHtml(color).Remove(0, 1);
        }

        private Color HtmlToColor(string html)
        {
            try
            {
                return ColorTranslator.FromHtml(html);
            }
            catch (System.ArgumentException e)
            {
                return Color.Empty;
            }
        }

        private void GenerateHueColors()
        {
            Color baseColor = Color.FromArgb(255, 255, 0, 0);

            for (int i = 0; i < 256; i++)
            {
                baseColor = Color.FromArgb(255, 255, 0, i);
                _hueColors.Add(baseColor);
            }

            for (int i = 0; i < 256; i++)
            {
                baseColor = Color.FromArgb(255, 255 - i, 0, 255);
                _hueColors.Add(baseColor);
            }

            for (int i = 0; i < 256; i++)
            {
                baseColor = Color.FromArgb(255, 0, i, 255);
                _hueColors.Add(baseColor);
            }

            for (int i = 0; i < 256; i++)
            {
                baseColor = Color.FromArgb(255, 0, 255, 255 - i);
                _hueColors.Add(baseColor);
            }

            for (int i = 0; i < 256; i++)
            {
                baseColor = Color.FromArgb(255, i, 255, 0);
                _hueColors.Add(baseColor);
            }

            for (int i = 0; i < 256; i++)
            {
                baseColor = Color.FromArgb(255, 255, 255 - i, 0);
                _hueColors.Add(baseColor);
            }
        }

        private void DrawHueColorsBitmap()
        {
            for (int i = 0; i < _hueColorsBitmap.Height; i++)
            {
                for (int j = 0; j < _hueColorsBitmap.Width; j++)
                {
                    _hueColorsBitmap.SetPixel(j, i, _hueColors[_hueColors.Count / _hueColorsBitmap.Height * i]);
                }
            }
        }

        private void GenerateGradient()
        {
            Color baseColor = GetHueColor();
            Color topLeft = Color.FromArgb(255, 255, 255, 255);
            Color bottom = Color.FromArgb(255, 0, 0, 0);

            for (int col = 0; col < 256; col++)
            {
                int R = topLeft.R - baseColor.R;
                int G = topLeft.G - baseColor.G;
                int B = topLeft.B - baseColor.B;
                Color calcColor = Color.FromArgb(255, topLeft.R - (int)(R / 255.0f * col), topLeft.G - (int)(G / 255.0f * col), topLeft.B - (int)(B / 255.0f * col));
                _gradientColors[0, col] = calcColor;
            }

            for (int row = 1; row < 256; row++)
            {
                int R = topLeft.R - bottom.R;
                int G = topLeft.G - bottom.G;
                int B = topLeft.B - bottom.B;
                Color newTopLeft = Color.FromArgb(255, topLeft.R - (int)(R / 255.0f * row), topLeft.G - (int)(G / 255.0f * row), topLeft.B - (int)(B / 255.0f * row));

                R = baseColor.R - bottom.R;
                G = baseColor.G - bottom.G;
                B = baseColor.B - bottom.B;
                Color newBaseColor = Color.FromArgb(255, baseColor.R - (int)(R / 255.0f * row), baseColor.G - (int)(G / 255.0f * row), baseColor.B - (int)(B / 255.0f * row));

                for (int col = 0; col < 256; col++)
                {
                    R = newTopLeft.R - newBaseColor.R;
                    G = newTopLeft.G - newBaseColor.G;
                    B = newTopLeft.B - newBaseColor.B;
                    Color calcColor = Color.FromArgb(255, newTopLeft.R - (int)(R / 255.0f * col), newTopLeft.G - (int)(G / 255.0f * col), newTopLeft.B - (int)(B / 255.0f * col));
                    _gradientColors[row, col] = calcColor;
                }
            }
        }

        private void DrawGradientBitmap()
        {
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 256; j++)
                {
                    _gradientBitmap.SetPixel(j, i, _gradientColors[i, j]);
                }
            }
        }

        private void Gradient_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Point p = e.GetPosition(this);
            System.Windows.Point relativePoint = Gradient.TransformToAncestor(ColorPickerName).Transform(new System.Windows.Point(0, 0));
            System.Windows.Point clickPoint = new System.Windows.Point(p.X - relativePoint.X, p.Y - relativePoint.Y);

            _selectedGradientIndices = new int[] { (int)clickPoint.X, (int)clickPoint.Y };
            ChangeColor(GetGradientColor(), ChangesSource.GRADIENT);
        }

        private void ColorGradient_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Point p = e.GetPosition(this);
            System.Windows.Point relativePoint = ColorGradient.TransformToAncestor(ColorPickerName).Transform(new System.Windows.Point(0, 0));
            System.Windows.Point clickPoint = new System.Windows.Point(p.X - relativePoint.X, p.Y - relativePoint.Y);

            _selectedHueColorIndex = (int)clickPoint.Y;
            DrawGradient();
            ChangeColor(GetGradientColor(), ChangesSource.GRADIENT);
        }

        private void RGBTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Regex regex = new Regex(@"[0-9]+");

                if (regex.IsMatch(RTextBox.Text) && regex.IsMatch(GTextBox.Text) && regex.IsMatch(BTextBox.Text))
                {
                    Color newColor = Color.FromArgb(255, Int32.Parse(RTextBox.Text), Int32.Parse(GTextBox.Text), Int32.Parse(BTextBox.Text));
                    ChangeColor(newColor, ChangesSource.RGB);
                }
            }
        }

        private void HexTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Regex regex = new Regex(@"[0-9A-F]+");

                if (regex.IsMatch(HexTextBox.Text))
                {
                    Color newColor = HtmlToColor("#" + HexTextBox.Text);
                    if (newColor != Color.Empty)
                    {
                        ChangeColor(newColor, ChangesSource.HEX);
                    }
                }
            }
        }

        private enum ChangesSource
        {
            RGB,
            HEX,
            GRADIENT,
            EXTERNAL
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
